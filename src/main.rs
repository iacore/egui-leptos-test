use std::time::{Duration, Instant};

use eframe::egui::{self, Button};
use leptos_reactive::{create_runtime, prelude::*, run_scope_undisposed, Scope, ScopeDisposer};

fn main() -> Result<(), eframe::Error> {
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(320.0, 240.0)),
        ..Default::default()
    };
    let (app, disposer) = MyApp::new();
    eframe::run_native("My egui App", options, Box::new(|_cc| Box::new(app)))
}

struct MyApp {
    timer_until: Option<Instant>,
    deg_f: (ReadSignal<f32>, WriteSignal<f32>),
    deg_c: (ReadSignal<f32>, WriteSignal<f32>),
    no_fahrenheit: bool,
    cx: Scope,
}

impl MyApp {
    fn new() -> (Self, ScopeDisposer) {
        let runtime = create_runtime();
        let (a, _scope_id, _scope_disposer) = run_scope_undisposed(runtime, |cx| {
            let deg_c = create_signal(cx, 0.);
            let deg_f = create_signal(cx, 0.);

            Self {
                cx,
                timer_until: None,
                deg_c,
                deg_f,
                no_fahrenheit: false,
            }
        });

        (a, _scope_disposer)
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.cx.untrack(|| {
            egui::CentralPanel::default().show(ctx, |ui| {
                ui.heading("Temp converter");

                const TIME_RATIO: f32 = 10.;
                let now = Instant::now();
                let mut timer_enabled = false;
                if let Some(q) = self.timer_until {
                    ctx.request_repaint();
                    self.deg_c.1((q - now).as_secs_f32() * TIME_RATIO);
                    if q <= now {
                        self.timer_until = None;
                    } else {
                        timer_enabled = true;
                    }
                }
                if ui
                    .add_enabled(!timer_enabled, Button::new("Add 1 °C"))
                    .clicked()
                {
                    self.deg_c.1(self.deg_c.0() + 1.0);
                }

                if ui
                    .add_enabled(!timer_enabled, Button::new("Start timer"))
                    .clicked()
                {
                    self.timer_until =
                        Some(now + Duration::from_secs_f32(self.deg_c.0() as f32 / TIME_RATIO));
                }
                if ui
                    .add_enabled(timer_enabled, Button::new("Stop timer"))
                    .clicked()
                {
                    self.timer_until = None;
                }

                ui.toggle_value(&mut self.no_fahrenheit, "No fahrenheit")
                    .changed();

                let mut deg_c = self.deg_c.0();
                if ui
                    .add_enabled(
                        !timer_enabled,
                        egui::Slider::new(&mut deg_c, 0.0..=120.0).text("°C"),
                    )
                    .changed()
                {
                    self.deg_c.1(deg_c);
                    self.deg_f.1(deg_c);
                }

                let mut deg_f = self.deg_f.0();
                if ui
                    .add_enabled(
                        !timer_enabled && !self.no_fahrenheit,
                        egui::Slider::new(&mut deg_f, 0.0..=120.0).text("°F"),
                    )
                    .changed()
                {
                    self.deg_f.1(deg_f);
                    self.deg_c.1(deg_f);
                }
            });
        });
    }
}
